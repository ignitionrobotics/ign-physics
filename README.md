# REPOSITORY MOVED

## This repository has moved to

https://github.com/ignitionrobotics/ign-physics

## Issues and pull requests are backed up at

https://osrf-migration.github.io/ignition-gh-pages/#!/ignitionrobotics/ign-physics

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/ign-physics

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

